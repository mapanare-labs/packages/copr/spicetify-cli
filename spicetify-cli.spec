%define debug_package %{nil}

Name:    spicetify-cli
Version: 2.38.5       
Release: 1%{?dist}
Summary: Command-line tool to customize Spotify client
Group:   Applications/System
License: LGPLv2+
Url:     https://github.com/spicetify/%{name}
Source0: %{url}/releases/download/v%{version}/spicetify-%{version}-linux-amd64.tar.gz
Source1: spicetify.sh

%description
Command-line tool to customize Spotify client

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/%{name}

install -m 755 spicetify %{buildroot}%{_datadir}/%{name}
install -m 644 globals.d.ts %{buildroot}%{_datadir}/%{name}
install -m 644 css-map.json %{buildroot}%{_datadir}/%{name}
cp -rv Themes Extensions jsHelper %{buildroot}%{_datadir}/%{name}
install -m 755 %{SOURCE1} %{buildroot}%{_bindir}/spicetify

%files
%{_bindir}/spicetify
%{_datadir}/%{name}/spicetify
%{_datadir}/%{name}/globals.d.ts
%{_datadir}/%{name}/css-map.json
%{_datadir}/%{name}/Themes/
%{_datadir}/%{name}/Extensions/
%{_datadir}/%{name}/jsHelper/

%changelog
* Thu Apr 11 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v2.33.2

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.28.1

* Wed Nov 15 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.27.1

* Wed Jul 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.22.1

* Mon Jun 19 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.20.1

* Thu May 18 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.18.1

* Mon May 08 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.18.0

* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.17.2

* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.16.1

* Fri Nov 25 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.14.3

* Tue Nov 01 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 2.14.1

* Mon Sep 12 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
